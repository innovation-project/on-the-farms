#!/usr/bin/python3
import os

# Update script.
print("Attempting automatic update...")
os.system("cd /home/pi/on-the-farms && git pull --allow-unrelated-histories")

print("Update finished! Running script.")
os.system("python3 /home/pi/on-the-farms/farmio.py 11 21")
