const int gasPin = A0;
const int uvPin = A7;
const int moisturePin = A3;


void setup()
{
    Serial.begin(9600);
}

void loop()
{
    Serial.print("[sensor_data]");
    // Print Moisture sensor data
    Serial.print("\nMoisture = ");
    Serial.print(analogRead(moisturePin));
    // Print Gas sensor data
    Serial.print("\nGas = ");
    Serial.print(analogRead(gasPin));
    // Print UV Light sensor data.
    Serial.print("\nUV = ");
    Serial.print(analogRead(uvPin));

    Serial.println();
    delay(500);
}
