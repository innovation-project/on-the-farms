#!/usr/bin/python3
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
print("\n-=##########=-\nWelcome to the Farm I/O sensor script. Please see https://getfarm.io for details.")

import sys, os, time
import Adafruit_DHT
import json, toml
import subprocess
import serial
import influxdb
import board, busio, adafruit_tsl2591

print("Attempting automatic update...")
os.system("cd /home/pi/on-the-farms && git pull")

class Pyphana:
    def __init__(self, host, port, user, password, database_name, box_id = "unknown"):
        self.database_name = database_name
        self.client = influxdb.InfluxDBClient(host, port, user, password, self.database_name)

    def payload(self, farm_name, data, device_id):
        self.client.write_points([{
            "measurement": farm_name,
            "tags": {
                "location": device_id,
            },
            "fields": data
        }])

f = open("/home/pi/database_secret.txt", "r")
database_secret = f.readline().strip()
f.close()

print("Database secret is: {}, '{}'".format(type(database_secret), database_secret))

pyphana_client = Pyphana("data.getfarm.io", 8086, "admin", database_secret, "sensors")

# Parse command line parameters.
sensor_args = { '11': Adafruit_DHT.DHT11,
                '22': Adafruit_DHT.DHT22,
                '2302': Adafruit_DHT.AM2302 }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
    sensor = sensor_args[sys.argv[1]]
    pin = sys.argv[2]
else:
    print('Usage: sudo ./Adafruit_DHT.py [11|22|2302] <GPIO pin number>')
    print('Example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO pin #4')
    sys.exit(1)

i2c = busio.I2C(board.SCL, board.SDA)
light_sensor = adafruit_tsl2591.TSL2591(i2c)

# Try to grab a sensor reading.  Use the read_retry method which will retry up
# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
while True:
    python_blob = {}
    print('\nWaiting for 10 seconds...')
    time.sleep(10)
    print('Reading from sensors now!')
    humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

    # Get data from Ardunio's serial.
    try:
        with open("/dev/ttyUSB0", "r", encoding="iso-8859-1") as ArduinoSerial:
            print('> Reading Arduino Serial Buffer...')
            toml_blob = ArduinoSerial.readline()
            for i in range(0,5):
                toml_blob = toml_blob + ArduinoSerial.readline()
            toml_blob = toml_blob.split('[')[1]
            toml_blob = "[" + toml_blob
            print(toml_blob)
        print('< Closed Arduino Serial Buffer.')

        toml_blob = toml_blob.replace('\n\n', '\n').strip()


        # Now we have valid TOML, let's load it into a Python_Dict.
        try:
            python_blob = toml.loads(toml_blob)['sensor_data']
        except toml.decoder.TomlDecodeError as e:
            print("Could not decode TOML, ignoring: {}".format(e))

        print("Arduino TOML to JSON: {}".format(python_blob))
    except FileNotFoundError:
         print("WARNING: Could not get sensor data from Arduino /dev/ttyUSB0")

    # Now we have a Python dictionary! Let's add our DHT-11 values.
    python_blob['Humidity'], python_blob['Temperature'] = humidity, temperature

    # Now let's add the values from our tsl2591 light sensor!
    try:
        python_blob['Light Lux'] = float(light_sensor.lux)
    except RuntimeError:
        print("WARNING: Could not get 'Light Lux' value, am I near the sun?")
    python_blob['Light Visible'] = light_sensor.visible
    python_blob['Light Infrared'] = light_sensor.infrared
    python_blob['Light Full Spectrum'] =  light_sensor.full_spectrum
    python_blob['Light Raw-Luminosity (All)'] = light_sensor.raw_luminosity[0]
    python_blob['Light Raw-Luminosity (IR-only)'] = light_sensor.raw_luminosity[1]

    print("Payload constructed, appending IP metadata...")
    ip_metadata = subprocess.check_output("curl ifconfig.co/json", shell=True).decode('UTF-8')
    python_blob = {**python_blob, **ip_metadata} # Merge dictionaries.

    pyphana_client.payload("Farm 1", python_blob, "DevBoard_001")

    # Convert Python_Dict to a JSON blob.
    json_blob = json.dumps(python_blob)

    print("Sent payload: {}".format(json_blob))
