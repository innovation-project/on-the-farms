#!/usr/bin/env python3

# don't forget to `pip install toml`
import os, time
try:
	pass
    #os.system('pip3 install --user -r requirements.txt') # Install dependencies from requirements.txt
except:
    print("Could not install toml/json. Check your internet connection and try again.")
    
import json, toml

time.sleep(5)
f = open("/dev/ttyUSB0", "r")
toml_blob = f.read() 
f.close()

toml_blob = toml_blob.split('\n\n')[0]
print(f"Debug:\n{toml_blob}")
toml_blob = toml.loads(toml_blob)

# Add DHT-11 Humidity/Temperature sensor data here to the Python dictionary toml_blob.

#toml_blob = {time.time(): {toml_blob}} # Add a key for the sensor data.
json_blob = json.dumps(toml_blob)

print(json_blob) # Output JSON
