import random
import time
import sys
import influxdb
# from influxdb import InfluxDBClient
class Pyphana:
	"""Connect the Farm IO JSON input into the database"""

	def __init__(self, host, port, user, password, database_name, box_id = "unknown"):
		self.database_name = database_name
		self.client = influxdb.InfluxDBClient(host, port, user, password, self.database_name)

	def payload(self, sensor_name, data_dict, unit_name = "unknown"):
		# Ensure that payload is in JSON Format and uses known tags
		self.client.write_points([
			{
				"measurement": sensor_name,
				"tags":{
					"location": unit_name,
				},
				"time": time.ctime(),
				"fields": data_dict
			},
		])
